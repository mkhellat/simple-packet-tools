# simple-packet-tools

A collection of simple tools for manipulating protocol data units

## sslsplit-executer

Execute sslsplit as an ssl offloader

### Run

The script should be run on a _GNU/Linux_ by _root_ since it needs to
append two nat rules to the PREROUTING chain. Clearly, you would need
an installation of _sslsplit_ and IP forwarding to be enabled and
supported in the kernel to be able to transparently offload traffic
passing through.


````
./sslsplit-executer configure
````

The script would then check the requirements, creating an sslsplit
project tree or anything missing there. Finally, the _default_
required PREROUTING chain nat rules would be added and sspplit is
executed listening on tcp port 8443 for TLS traffic and 8080 for
nonTLS traffic.

If you want to change the default configuration, refer to the help
message.

````
./sslsplit-executer --help
````

Changes made by the script could be reverted easily.

````
./sslsplit-executer clean --revert-all
````

## tcp-packet-player

Send new packet(s) inside a tcp connection

### Run

This is a python3 program and would require root privileges for
opening sockets.

````
./tcp-packet-player
````
Upon entering the above command, program would aske you for the tcp
connection port number (server side or client side), number of packets
to be crafted, and hex string to be used as the tcp payload.

You could feed all the required information as arguments to the scrpt.


# GNU GPL v3+

Copyright (C) 2023 Mohammadreza Khellat GNU GPL v3+

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

See also https://www.gnu.org/licenses/gpl.html
